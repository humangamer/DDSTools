﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManagedDDSTools_Native;
using System.Runtime.InteropServices;

namespace DDSTools
{
    public static class DDS
    {
        public static byte[] CompressTexture(byte[] source, int width, int height, DDSCompression compression)
        {
            byte[] output = new byte[width * height * 20];

            int[] outSize = new int[] { output.Length };
            GCHandle outSizeHandle = GCHandle.Alloc(outSize, GCHandleType.Pinned);

            GCHandle outHandle = GCHandle.Alloc(output, GCHandleType.Pinned);
            GCHandle srcHandle = GCHandle.Alloc(source, GCHandleType.Pinned);
            
            if (!DDSNative.CompressTexture(srcHandle.AddrOfPinnedObject(), width, height, outHandle.AddrOfPinnedObject(), outSizeHandle.AddrOfPinnedObject(), (DDSNative.Compression)compression))
            {
                return null;
            }

            byte[] result = new byte[outSize[0]];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = output[i];
            }

            srcHandle.Free();
            outHandle.Free();
            outSizeHandle.Free();

            return result;
        }

        public static byte[] DecompressTexture(byte[] source, int width, int height, DDSCompression compression)
        {
            int outSize = width * height * 4;
            byte[] output = new byte[outSize];

            GCHandle outHandle = GCHandle.Alloc(output, GCHandleType.Pinned);
            GCHandle srcHandle = GCHandle.Alloc(source, GCHandleType.Pinned);

            if (!DDSNative.DecompressTexture(srcHandle.AddrOfPinnedObject(), source.Length, outHandle.AddrOfPinnedObject(), width, height, (DDSNative.Compression)compression))
            {
                return null;
            }

            srcHandle.Free();
            outHandle.Free();

            return output;
        }
    }

    public enum DDSCompression
    {
        DXT1,
        DXT3,
        DXT5
    }
}
